import pywapi
import datetime
import dateutil.parser

from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        outcome = request.json['outcome']
        location="Paris"#Default
        date=datetime.datetime.now()#Default
        weather_date_today = datetime.datetime.now().strftime("%b %d")
        weather_date = weather_date_today
        if 'entities' in outcome:
            if 'location' in outcome['entities']:
                if len(outcome['entities']['location']) > 0:
                    if 'value' in outcome['entities']['location'][0]:
                        tmp_location = outcome['entities']['location'][0]['value']
                        if tmp_location != "outside":
                            location = tmp_location
            if 'datetime' in outcome['entities']:
                if len(outcome['entities']['datetime']) > 0:
                    if 'value' in outcome['entities']['datetime'][0]:
                        date = dateutil.parser.parse(outcome['entities']['datetime'][0]['value'])
                        weather_date = date.strftime('%b %d')
        location_id = ""
        locations_ids=pywapi.get_loc_id_from_weather_com(location)
        if locations_ids['count'] > 0:
            location_id = locations_ids[0][0]
        if location_id != "":
            weather = pywapi.get_weather_from_weather_com(location_id)
            if (weather_date == weather_date_today):
                answer = "It is " + weather['current_conditions']['text'].lower() + " and the temperature is " + weather['current_conditions']['temperature'] + " degrees celsius, now in " + location
            else:
                if 'forecasts' in weather:
                    found = False
                    for day in weather['forecasts']:
                        print("Checking <" + day['date'] + "> on <" + weather_date + ">")
                        if day['date'] == weather_date:
                            answer = "Day : " + day['day']['text'].lower() + ", night : " + day['night']['text'].lower() + ", high : " + day['high'] + " degrees celsius, low : " + day['low'] + " degrees celsius, sunrise : " + day['sunrise'] + ", sunset : " + day['sunset']
                            found = True
                    if found == False:
                        answer = "Cannot get a forecast so far in time"
                else:
                    answer = "Forecast not available"
        else:
            answer = "I can't find what you are looking for"
        return answer

if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
